.. blog documentation master file, created by
   sphinx-quickstart on Tue Nov 10 08:57:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to blog's documentation!
================================

Gitlab: https://gitlab.com/X4oc/blog

.. toctree::
   :maxdepth: 2

   dirsrc/tutorial/tutorial.rst
   dirsrc/how-to/how-to.rst
   dirsrc/reference/reference.rst
   dirsrc/explanation/explanation.rst



