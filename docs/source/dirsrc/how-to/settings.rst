How to edit settings
====================

Blog settings
-------------

Go to http://localhost:6174/admin/settings
A form with current settings is shown. Update the settings while keeping the same type and click the UPDATE button. Restart after updating.

pathAdmin
^^^^^^^^^
Path to the admin part of the blog app. After editing that field to <some_string>, the path to admin becomes http://localhost:6174/some_string

Type: String

email
^^^^^
Email of the Contact page.

Type: String

twitter
^^^^^^^
URL of the twitter account of the author.

Type: String

rss
^^^
Not used.

Type: String

imgTitle
^^^^^^^^
URL of the image on the top.

Type: String

lastNArticles
^^^^^^^^^^^^^
Number of articles which appear on the home page.

Type: Number

lastNCommentaries
^^^^^^^^^^^^^^^^^
Number of commentaries which appear on the bottom part.

Type: Number

maxBlogRollItems
^^^^^^^^^^^^^^^^
Number of blog roll items which appear on the bottom part.

Type: Number

.. _Moderation settings:

moderationCommentaryAuthor
^^^^^^^^^^^^^^^^^^^^^^^^^^
Message replacing the author field of a moderated comment.

Type: String

moderationCommentaryText
^^^^^^^^^^^^^^^^^^^^^^^^
Message replacing the text field of a moderated comment.

Type: String

Blog default settings
---------------------

Edit file blog-backend-graphql/src/main/resources/blog.properties

Installation settings
---------------------

See :ref:`Modify the behavior of the installation<Modify the default behavior>`