How to access the services
==========================

Requirements:
#. The 3 services should be running

Database service
----------------

.. code-block:: sh

    docker exec -it blog_db_1 mysql -uroot -p

The default root password is: root
Type \h for help and \q to quit.

Backend service: blog-backend-graphql
-------------------------------------

Use GraphiQL, a graphical interactive in-browser GraphQL IDE:
https://github.com/graphql/graphiql

Connect to http://localhost:8080/grephiql

See the GraphQL back end API documentation:
:ref:`backend-api`

Front end service: blog-frontend-apollo
---------------------------------------
In browser, install extension: React Developer tools
In browser Web Developer tools, go to "Components" tab
