How to compile the sources individually
=======================================


Steps to Setup the Blog back end app (blog-backend-graphql)
-----------------------------------------------------------
Clone the application
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: sh

    git clone https://gitlab.com:X4oc/blog.git
    cd blog-backend-graphql

Create MySQL database
^^^^^^^^^^^^^^^^^^^^^
create database blog

Change MySQL username and password as per your MySQL installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    open src/main/resources/application.properties file.

    change spring.datasource.username and spring.datasource.password properties as per your mysql installation

Run the app
^^^^^^^^^^^
You can run the spring boot app by typing the following command

.. code-block:: sh

    mvn spring-boot:run

The server will start on port 8080.

You can also package the application in the form of a jar file and then run it like so

.. code-block:: sh

    mvn package
    java -jar target/blogbackend-0.0.1-SNAPSHOT.jar

Steps to Setup the React front end app (blog-frontend-apollo)
-------------------------------------------------------------

First go to the blog-frontend-apollo folder

.. code-block:: sh

    cd blog-frontend-apollo

Then type the following command to install the dependencies and start the application

.. code-block:: sh

    npm install && npm start

The front-end server will start on port 3000.