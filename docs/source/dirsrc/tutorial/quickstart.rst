Quickstart
==========

Requirements
^^^^^^^^^^^^

#. Docker installed: https://docs.docker.com/get-docker/
#. Docker-compose installed: https://docs.docker.com/compose/install/
#. Ports 8080 and 6174 not used: https://www.cyberciti.biz/faq/unix-linux-check-if-port-is-in-use-command/
#. Internet connection (at least the first time and if modifying the default behavior, see below)

Clone the application
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: sh

    git clone https://gitlab.com:X4oc/blog.git

Launch App with Docker Compose
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, the blog will be empty of any article, comment or blog roll.
The successive launches of the app keep the data in memory.

.. code-block:: sh

    docker-compose up

It will take some time the first time.
Wait until a line with "ready" appears at the bottom of the screen.

Three services should be running:

#. blog_blog-app-client_1
#. blog_blog-app-server_1
#. blog_db_1

Browse the blog app
^^^^^^^^^^^^^^^^^^^
While the app is running, connect to http://localhost:6174/

Stop App
^^^^^^^^

Gracefully stopping
"""""""""""""""""""

Press Ctrl+C

Force stopping (if necessary)
"""""""""""""""""""""""""""""

Press Ctrl+C again

Stop the 3 services in Docker
"""""""""""""""""""""""""""""

.. code-block:: sh

    docker-compose down

.. _Modify the default behavior:

Modify the default behavior
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The 3 services should be stopped before.

To launch each time on clean memory
""""""""""""""""""""""""""""""""""""

In file docker-compose.yml, remove the line (or comment it by prefixing it with #)

.. code-block:: yaml

    SPRING_JPA_HIBERNATE_DDL-AUTO: update

Run the app with that command instead:

.. code-block:: sh

    docker-compose up --build --force-recreate

To populate the blog with some random data
""""""""""""""""""""""""""""""""""""""""""

In file docker-compose.yml, remove the line (or comment it by prefixing it with #)

.. code-block:: yaml

    FILL_DB_RANDOM: 'false'

Run the app with that command instead:

.. code-block:: sh

    docker-compose up --build --force-recreate