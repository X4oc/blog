Using the blog as a reader
==========================

You can navigate between pages with the navigation bar on the top. If some info is saved by clicking on a button, restart the page to make the changes appear.

Home page
---------
In the main part under the navigation bar, you can see the last articles published.
Under it, there are four boxes with some updated info about the last interactions with the blog: comments, articles, categories (of articles) and blog roll.
Click on "Read more" on some article abstract to go to the selected article page.

.. _Article Page:

Article page
------------

Displays the selected article with full text and commenting feature.
Use the commenting form below to add a comment.
If some comment is clicked in the grey display before adding a commentary, the latter will be replying to the former. If your comment should just be appended at the end of the comments, don't click on any comment in the grey area (restart the page if necessary to lose focus).

Other features
--------------

The reader can browse the other pages to get some info:

#. About is info about the author
#. Contact is how to contact the author
#. Twitter is an external link to Twitter
