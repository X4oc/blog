Using the blog as the author
============================

The author of the blog is the unique writer who adds content to the site (except for comments). The administrative half of the blog is dedicated to manage the new entries, comments, blog roll and other settings.

Go to http://localhost:6174/admin to make appear another line in the navigation bar. By default, you see the dashboard with the current stats of the blog.

Articles
--------

An article can be added, updated or deleted.

Adding an article
^^^^^^^^^^^^^^^^^

Click on the + icon in the top-left corner of the table.
A popup appears, with 3 mandatory fields to fill.
Next to the Add button, you can activate a switch to add multiple articles at once.
Restart after adding.

Editing an article
^^^^^^^^^^^^^^^^^^
In that order:

#. Modify the info about one of more articles
#. Select them with the left boxes
#. An UPDATE button appears on the top-right corner of the table. Click on it.

Restart after updating.

Deleting an article
^^^^^^^^^^^^^^^^^^^

Select one or more articles to delete with the left boxes
A DELETE button appears on the top-right corner of the table. Click on it.
Restart after deleting.

Comments
--------

The comments table is the only one that shows the underlying database IDs. By looking at the parentId value of a comment, you can understand to which comment it is replying. A value of 1 indicates that the comment is not replying to anyone.
A comment can be updated or moderated. Adding a comment can be done only on an article page. cf. :ref:`Article Page`

Editing a comment
^^^^^^^^^^^^^^^^^

In that order:

#. Modify the info about one of more comments
#. Select them with the left boxes
#. An UPDATE button appears on the top-right corner of the table. Click on it.

Restart after updating.

Moderating an comment
^^^^^^^^^^^^^^^^^^^^^

Select one or more comments to moderate with the left boxes
A MODERATE button appears on the top-right corner of the table. Click on it.
Restart after moderating.

PS: The default messages of moderation can be modified in the :ref:`settings<Moderation settings>`.

Blog Roll
---------

An blog roll item can be added, updated or deleted.
The category of the blog roll item is used to organize the blog roll as you wish.

Adding a blog roll item
^^^^^^^^^^^^^^^^^^^^^^^

Click on the + icon in the top-left corner of the table.
A popup appears, with 3 mandatory fields to fill.
Next to the Add button, you can activate a switch to add multiple blog roll items at once.
Restart after adding.

Editing a blog roll item
^^^^^^^^^^^^^^^^^^^^^^^^
In that order:

#. Modify the info about one of more blog roll items
#. Select them with the left boxes
#. An UPDATE button appears on the top-right corner of the table. Click on it.

Restart after updating.

Deleting a blog roll item
^^^^^^^^^^^^^^^^^^^^^^^^^

Select one or more blog roll items to delete with the left boxes
A DELETE button appears on the top-right corner of the table. Click on it.
Restart after deleting.