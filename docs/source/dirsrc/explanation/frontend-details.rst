.. _Frontend implementation details:

Frontend implementation details
===============================

Common patterns
---------------

App
^^^

The entry point of the app, called by index (entry point of React, React.StrictMode is disabled to prevent some cryptic errors to appear).

Gets the blog properties which are forwarded throughout the app.
Renders the AppBar (material-ui), the Navigation, the Main and the Footer.

Likewise, AppAdmin is the entry point of the admin part of the app.
Renders the NavigationAdmin and the MainAdmin

Main
^^^^

The Main switches between routes (selected by the path) and selects which component to render accordingly. If the path is dynamic, the route has not the "exact" attribute. It forwards the props if necessary.

Idem with MainAdmin.

Navigation
^^^^^^^^^^

It uses a Paper component (material-ui) with Tabs, to show the links to the different pages. It stores a "value" in its state to show which one is currently selected.

Idem for NavigationAdmin. The NavigationAdmin appears under the Navigation for convenience for the admin.

Home
^^^^

The home page is routed to the root of the path. For the readers part, it shows the last articles published.

For the admin part, DashboardAdmin shows the Dashboard built in the server and sent with GraphQL.

Common components
-----------------

Footer
^^^^^^

Shows different info about the data in 4 boxes (material-ui) inside a Grid (material-ui). Only the articles categories don't need to be rendered with a component. Each of the other 3 boxes uses a List (material-ui).

GlobalFilter & TablePaginationActions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

From the kitchen sink example (see :ref:`React Table`), those two classes could be refactored to be used by multiple components in the admin part of the app (ArticleAdmin, BlogRollAdmin, CommentaryAdmin).

The other ones (EnhancedTable, TableToolbar and all the buttons) could not be refactored because of the React Rules of Hooks which prevent the useQuery from being called after a handler method is passed to a child component.

Everything in those 4 classes is not well understood up to now, maybe there is a way?

Readers part (user)
-------------------

Article
^^^^^^^

Shows a complete article with comments on some dynamic route. Forwards the commentaries to the CommentaryTree

Uses the database ID in the path which is not preferable if the URL is used outside of the application.

CommentaryTree
^^^^^^^^^^^^^^
It gets the comments from the Article, converts them to a suitable format for the react-tree Tree rendering and adds a CommentaryForm.

Writer part (admin)
-------------------

ArticleAdmin, BlogRollAdmin, CommentaryAdmin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All those components use the React Table component. The resulting components code is heavily duplicated.

Each one uses a Table which initializes the columns and the data to be displayed. Then it calls an EnhancedTable that coordinates all data and handlers. The IndeterminateCheckbox is used to select one or more rows. Then a TableToolbar is displayed in the top of the table, with some buttons to click on.

SettingsAdmin
^^^^^^^^^^^^^

Shows the settings form with default values from the props. Nothing special except in the updateSettings handler, a line is left commented

.. code-block:: javascript

    //window.location.reload();

When uncommented, the page is well reloaded but before the call to update on the above line, which remains a mystery.