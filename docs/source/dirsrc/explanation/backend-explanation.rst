Backend explanation
===================

Persistence
-----------

The app should persist data (article, comment, blog roll item) which can be represented in tables.
The operations being entirely CRUD, the Spring Data is suitable. There is no need for a domain layer in this project and entities are modeled with composition in mind to reduce mutability.
The properties of the blog are persisted in a file which can be more easily modified between runs.

GraphQL
-------
The data is exposed with GraphQL on the only endpoint. GraphQL was chosen to replace REST in this project because in the frontend, multiple requests were sent for one component and it simplified greatly that part.
The GraphQL type system in general does not define any form of inheritance. Some input objects looks like entity duplicates but it keeps the spec simple. Some extended scalars are added for the date/time fields.
The graphql-java-tools provides the mapping between the Java services and the GraphQL schema. That schema is actually the API of the backend.

Other
-----

The lorem package allows to generate some random content if needed.

Implementation details
----------------------

.. toctree::
   :maxdepth: 1

   backend-details.rst