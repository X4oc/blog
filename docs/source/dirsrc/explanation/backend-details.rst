.. _Backend implementation details:

Backend implementation details
==============================

Persistence
-----------

Entity
^^^^^^

Entities were thought first immutable and composable (no list or tree of comments in articles), then the mutability was added. Coincidentally, the structure of Comment is easily consumed in the tree display in the front end.

Apart from classic entity POJO, the public constructors for entities are numerous:

#. JPA needs the default one
#. the one without id field is intended to be the proper constructor
#. the one with id field is necessary for converting the equivalent input objects in GraphQL to the entity ones. An alternative was tried by making that last constructor private and declaring the GraphQL input objects in the dao layer but it was polluting more that way.

Repository
^^^^^^^^^^

Classic JpaRepository operations. Request objects are often containing ID instead of whole objects to deter cascading issues, so there are multiple methods of find by Key.

The methods for updating or deleting a whole collection of entities were not used because the conversion from the GraphQL Input object to the entity one was nedded beforehand so it is just a matter of clarity of the service methods.

Some methods use a PageRequest functionality for fetching pages of data.

Web service
-----------

Service
^^^^^^^

A big part of the service layer is just mapping the operations of the repository to those of the GraphQL resolvers (Query and Mutation) and adding the transactional constraint. It was made the most functionnaly possible and readable.

Because of the relationship between an Article and some Commentaries, the service of adding and deleting an article are more complex.

A SumCommentaries instance keeps in memory just a map of the count of commentaries per article to mitigate the number of database queries (info used in the home page, more viewed than a particular page).

A Dashboard object condenses relevant info for the admin home page.

Query
^^^^^

No Input type for queries so it is mostly forwarding the services to GraphQL.

The SumCommentariesEntry object is needed because GraphQL doesn't support sending maps of data. The map is rebuilt in the frontend with pure javascript function Map().

Mutation
^^^^^^^^

Input types, not inheriting from one another, respect the specification of GraphQL to keep types simple. The need of those input types comes from the persistence layer which can only save/delete entities and the clarity obtained from forwarding the data from the client to the database. An alternative was tried to pass all info by primitive fields but it was cumbersome. In the implementation, the input types are defined as static nested classes in their respective mutation class.

Apart from the input types, it doesn't differ from queries.

Other
-----

In global package, some configuration objects are defined and a CommandLineRunner in Initialization initializes the root commentary (to avoid null at all costs). If asked, some random data is saved (if the application property fill.db.random is true).

In the main application class, a "ready" message appears after initialization. The CORS is configured.

No tests. No custom errors.

