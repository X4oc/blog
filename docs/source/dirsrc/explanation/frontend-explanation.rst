Frontend explanation
====================

React
-----

React Hooks and data fetching
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

React Hooks new way of defining components prevents the use of soon-to-be deprecated lifecycle methods like componentWillMount. Before using GraphQl, the frontend was fetching data with axios and a REST API served by Spring @RestController. The first render was empty and a componentWillUpdate was used to fetch the data. Then a second render showed the data. The complexity arose when multiple requests were necessary to render one component. The combination of GraphQL and Apollo client (with useQuery method) was successful to centralize the fetching process. Now no multiple renders are called by React on one component.

React Router
^^^^^^^^^^^^

This Single Page Application uses React Router to define the routes to all relevant components. It can forward the props of the parent component so the logic of composing the components remains. But forwarding some arbitrary functions can interfere with the `React Rules of Hooks <https://reactjs.org/docs/hooks-rules.html>`_

Data visualisation
------------------

React-tree
^^^^^^^^^^

Apart from minor changes (assign the root comments parentId to null, adding a label, etc.), the structure of the comments is used to display a tree of comments with replying feature. The depth of the replies is virtually not limited. See https://github.com/naisutech/react-tree

.. _React Table:

React Table
^^^^^^^^^^^

This structure without any CSS consideration appears to be well suited for projects where the presentation is secondary. The complete code using a single table can be quite bloated (with the static definition of every column, the size of the render menthod, etc.) but apparently, it is quite common for that functionality. Here, the React Rule of Hooks makes everything more complex because the fetching using useQuery could not appear after some handler functions passed from a layer to another. The example followed in this app is here: https://github.com/tannerlinsley/react-table/tree/master/examples/kitchen-sink It was chosen because it integrated client side pagination, sorting, global search, add row, delete row and with Material UI.

Other
-----

MaterialUI
^^^^^^^^^^
A framework of React components used throughout in the blog app. Only specific components provided by other libraries may not follow that design (example: React-tree).

Luxon
^^^^^
It wraps Javascript dates and times. See https://moment.github.io/luxon/index.html

React Truncate
^^^^^^^^^^^^^^
It allows texts to be truncated. It is used in article insights in the home page. See https://github.com/pablosichert/react-truncate

Implementation details
----------------------

.. toctree::
   :maxdepth: 1

   frontend-details.rst
