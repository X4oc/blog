package com.x4oc.blogbackend.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.x4oc.blogbackend.global.BlogProperties;
import com.x4oc.blogbackend.service.BlogPropertiesService;
import org.springframework.stereotype.Component;

@Component
public class BlogPropertiesMutation implements GraphQLMutationResolver {
    private final BlogPropertiesService blogPropertiesService;

    static class BlogPropertiesInput extends BlogProperties {}

    public BlogPropertiesMutation(BlogPropertiesService blogPropertiesService) {
        this.blogPropertiesService = blogPropertiesService;
    }

    public BlogProperties setBlogProperties(BlogPropertiesInput input){
        return blogPropertiesService.set(input);
    }
}
