package com.x4oc.blogbackend.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.x4oc.blogbackend.dao.entity.BlogRollItem;
import com.x4oc.blogbackend.service.BlogRollItemService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BlogRollItemMutation implements GraphQLMutationResolver {
    public static class BlogRollItemInput extends BlogRollItem{

        public BlogRollItem toBlogRollItem(){
            return new BlogRollItem(getId(), getTitle(), getUrl(), getCategory());
        }
    }

    private final BlogRollItemService blogRollItemService;

    public BlogRollItemMutation(BlogRollItemService blogRollItemService) {
        this.blogRollItemService = blogRollItemService;
    }

    public BlogRollItem createBlogRollItem(String title, String url, String category){
        return blogRollItemService.create(title, url, category);
    }

    public List<Long> deleteBlogRollItems(List<BlogRollItemInput> blogRollItems){
        return blogRollItemService.deleteBlogRollItems(blogRollItems);
    }

    public List<BlogRollItem> updateBlogRollItems(List<BlogRollItemInput> blogRollItems){
        return blogRollItemService.updateBlogRollItems(blogRollItems);
    }

}
