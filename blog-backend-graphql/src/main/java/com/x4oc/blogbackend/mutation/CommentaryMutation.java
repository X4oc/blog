package com.x4oc.blogbackend.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.x4oc.blogbackend.dao.entity.Article;
import com.x4oc.blogbackend.dao.entity.Commentary;
import com.x4oc.blogbackend.service.CommentaryService;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

@Component
public class CommentaryMutation implements GraphQLMutationResolver {
    private final CommentaryService commentaryService;
    public static class CommentaryInput {
        private long id;
        private String author;
        private Instant date;
        private String text;
        private long articleId;
        private long parentId;

        public CommentaryInput() {
        }

        public CommentaryInput(long id, String author, Instant date, String text, long articleId, long parentId) {
            this.id = id;
            this.author = author;
            this.date = date;
            this.text = text;
            this.articleId = articleId;
            this.parentId = parentId;
        }

        public long getId() {
            return id;
        }

        public String getAuthor() {
            return author;
        }

        public Instant getDate() {
            return date;
        }

        public String getText() {
            return text;
        }

        public long getArticleId() {
            return articleId;
        }

        public long getParentId() {
            return parentId;
        }

        public Commentary toCommentary(Article article, Commentary parent){
            return new Commentary(id, author, date, text, article, parent);
        }
    }

    public CommentaryMutation(CommentaryService commentaryService) {
        this.commentaryService = commentaryService;
    }

    public Commentary createCommentary(String author, String text, long articleId, long parentId){
        return commentaryService.create(author, text, articleId, parentId);
    }

    public List<Commentary> updateCommentaries(List<CommentaryInput> commentaries){
        return commentaryService.updateCommentaries(commentaries);
    }

}
