package com.x4oc.blogbackend.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.x4oc.blogbackend.dao.entity.Article;
import com.x4oc.blogbackend.service.ArticleService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ArticleMutation implements GraphQLMutationResolver {
    public static class ArticleInput extends Article{

        public Article toArticle(){
            return new Article(getId(), getTitle(), getDateCreation(), getDateLastModified(), getCategory(), getText());
        }
    }

    private final ArticleService articleService;

    public ArticleMutation(ArticleService articleService) {
        this.articleService = articleService;
    }

    public Article createArticle(String title, String category, String text){
        return articleService.create(title, category, text);
    }

    public List<Long> deleteArticles(List<ArticleInput> articles){
        return articleService.deleteArticles(articles);
    }

    public List<Article> updateArticles(List<ArticleInput> articles){
        return articleService.updateArticles(articles);
    }


}