package com.x4oc.blogbackend.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.x4oc.blogbackend.dao.entity.Commentary;
import com.x4oc.blogbackend.service.CommentaryService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CommentaryQuery implements GraphQLQueryResolver {

    private final CommentaryService commentaryService;

    public CommentaryQuery(CommentaryService commentaryService) {
        this.commentaryService = commentaryService;
    }

    public List<Commentary> getAllCommentaries(long count){
        return commentaryService.getAll(count);
    }

    public List<Commentary> getLastCommentaries(){
        return commentaryService.getLastCommentaries();
    }

    public List<Commentary> getLastNCommentaries(int limit){
        return commentaryService.getLastNCommentaries(limit);
    }

    public Optional<Commentary> getCommentary(long id){
        return commentaryService.get(id);
    }

    public List<Commentary> getAllCommentariesFromParent(long articleId, long parentId, long count){
        return commentaryService.getAllFromParent(articleId, parentId, count);
    }

    public long countCommentariesByArticle(long articleId){
        return commentaryService.countByArticle(articleId);
    }

    public List<SumCommentariesEntry> getSumCommentaries(long last){
        return commentaryService.getSumCommentaries(last);
    }

    public List<Commentary> getAllCommentariesFromArticle(long articleId, int count){
        return commentaryService.getAllCommentariesFromArticle(articleId, count);
    }
}
