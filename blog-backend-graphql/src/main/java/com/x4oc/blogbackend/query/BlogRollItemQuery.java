package com.x4oc.blogbackend.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import com.x4oc.blogbackend.dao.entity.BlogRollItem;
import com.x4oc.blogbackend.service.BlogRollItemService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;


@Component
public class BlogRollItemQuery implements GraphQLQueryResolver {

    private final BlogRollItemService blogRollItemService;

    public BlogRollItemQuery(BlogRollItemService blogRollItemService) {
        this.blogRollItemService = blogRollItemService;
    }

    public List<BlogRollItem> getNBlogRollItems(long count){
        return blogRollItemService.getNBlogRollItems(count);
    }

    public List<BlogRollItem> getBlogRollItems(){
        return blogRollItemService.getBlogRollItems();
    }

    public Optional<BlogRollItem> getBlogRollItem(long id){
        return blogRollItemService.get(id);
    }

    public List<BlogRollItem> findAllBlogRollItemsByCategory(String category, long count){
        return blogRollItemService.findAllBlogRollItemsByCategory(category, count);
    }
}
