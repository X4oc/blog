package com.x4oc.blogbackend.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.x4oc.blogbackend.global.BlogProperties;
import com.x4oc.blogbackend.service.BlogPropertiesService;
import org.springframework.stereotype.Component;

@Component
public class BlogPropertiesQuery implements GraphQLQueryResolver {

    private final BlogPropertiesService blogPropertiesService;

    public BlogPropertiesQuery(BlogPropertiesService blogPropertiesService) {
        this.blogPropertiesService = blogPropertiesService;
    }

    public BlogProperties getBlogProperties(){
        return blogPropertiesService.get();
    }

}
