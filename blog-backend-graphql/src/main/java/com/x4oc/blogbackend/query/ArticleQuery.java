package com.x4oc.blogbackend.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.x4oc.blogbackend.dao.entity.Article;
import com.x4oc.blogbackend.service.ArticleService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ArticleQuery implements GraphQLQueryResolver {

    private final ArticleService articleService;

    public ArticleQuery(ArticleService articleService) {
        this.articleService = articleService;
    }

    public List<Article> getAllArticles(){
        return articleService.getAllArticles();
    }

    public List<Article> getLastArticles(){
        return articleService.getLastArticles();
    }

    public List<Article> getLastNArticles(int limit) {
        return articleService.getLastNArticles(limit);
    }

    public Optional<Article> getArticle(long id){
        return articleService.get(id);
    }

    public List<String> getAllCategories(){
        return articleService.getAllCategories();
    }
}
