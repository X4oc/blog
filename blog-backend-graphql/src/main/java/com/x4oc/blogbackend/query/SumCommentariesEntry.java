package com.x4oc.blogbackend.query;

public class SumCommentariesEntry {
    private long articleId;
    private long sum;

    public SumCommentariesEntry(long articleId, long sum) {
        this.articleId = articleId;
        this.sum = sum;
    }
}
