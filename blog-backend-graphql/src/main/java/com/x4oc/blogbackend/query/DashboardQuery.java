package com.x4oc.blogbackend.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.x4oc.blogbackend.service.Dashboard;
import com.x4oc.blogbackend.service.DashboardService;
import org.springframework.stereotype.Component;

@Component
public class DashboardQuery implements GraphQLQueryResolver {

    private final DashboardService dashboardService;

    public DashboardQuery(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    public Dashboard getDashboard(){
        return dashboardService.getDashboard();
    }
}
