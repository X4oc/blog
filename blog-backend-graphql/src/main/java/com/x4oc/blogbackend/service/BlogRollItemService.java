package com.x4oc.blogbackend.service;

import com.x4oc.blogbackend.dao.entity.BlogRollItem;
import com.x4oc.blogbackend.dao.repository.BlogRollItemRepository;
import com.x4oc.blogbackend.global.BlogPropertiesConfiguration;
import com.x4oc.blogbackend.mutation.BlogRollItemMutation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BlogRollItemService {

    private final BlogRollItemRepository blogRollItemRepository;
    private final BlogPropertiesConfiguration blogPropertiesConfiguration;

    public BlogRollItemService(BlogRollItemRepository blogRollItemRepository, BlogPropertiesConfiguration blogPropertiesConfiguration) {
        this.blogRollItemRepository = blogRollItemRepository;
        this.blogPropertiesConfiguration = blogPropertiesConfiguration;
    }

    @Transactional
    public BlogRollItem create(String title, String url, String category){
        return blogRollItemRepository.save(new BlogRollItem(title, url, category));
    }

    @Transactional(readOnly = true)
    public List<BlogRollItem> getNBlogRollItems(long count){
        return blogRollItemRepository.findAll().stream().limit(count).collect(Collectors.toList());
    }

    public List<BlogRollItem> getBlogRollItems(){
        return getNBlogRollItems(blogPropertiesConfiguration.getMaxBlogRollItems());
    }

    @Transactional(readOnly = true)
    public Optional<BlogRollItem> get(long id){
        return blogRollItemRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public List<BlogRollItem> findAllBlogRollItemsByCategory(String category, long count){
        return blogRollItemRepository.findAllByCategory(category).stream().limit(count).collect(Collectors.toList());
    }

    @Transactional
    public List<Long> deleteBlogRollItems(List<BlogRollItemMutation.BlogRollItemInput> blogRollItems) {
        return blogRollItems.stream()
                .map(BlogRollItemMutation.BlogRollItemInput::toBlogRollItem)
                .peek(blogRollItemRepository::delete)
                .map(BlogRollItem::getId)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<BlogRollItem> updateBlogRollItems(List<BlogRollItemMutation.BlogRollItemInput> blogRollItems) {
        return blogRollItems.stream()
                .map(BlogRollItemMutation.BlogRollItemInput::toBlogRollItem)
                .map(blogRollItemRepository::save)
                .collect(Collectors.toList());
    }
}
