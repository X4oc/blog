package com.x4oc.blogbackend.service;

import com.x4oc.blogbackend.query.SumCommentariesEntry;

import java.util.List;

public class Dashboard {

    private final long sumArticles;
    private final long sumCommentaries;
    private final List<SumCommentariesEntry> sumCommentariesPerArticle;
    private final long sumBlogRollEntries;

    public Dashboard(long sumArticles, long sumCommentaries, List<SumCommentariesEntry> sumCommentariesPerArticle, long sumBlogRollEntries) {
        this.sumArticles = sumArticles;
        this.sumCommentaries = sumCommentaries;
        this.sumCommentariesPerArticle = sumCommentariesPerArticle;
        this.sumBlogRollEntries = sumBlogRollEntries;
    }
}
