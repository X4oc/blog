package com.x4oc.blogbackend.service;

import com.x4oc.blogbackend.global.BlogProperties;
import com.x4oc.blogbackend.global.BlogPropertiesConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BlogPropertiesService {
    private BlogProperties blogProperties;

    public BlogPropertiesService(BlogPropertiesConfiguration blogPropertiesConfiguration) {
        this.blogProperties = new BlogProperties(blogPropertiesConfiguration);
    }

    @Transactional(readOnly = true)
    public BlogProperties get(){
        return blogProperties;
    }

    @Transactional
    public BlogProperties set(BlogProperties blogProperties){
        this.blogProperties = blogProperties;
        return blogProperties;
    }
}
