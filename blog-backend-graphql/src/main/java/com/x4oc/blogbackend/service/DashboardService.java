package com.x4oc.blogbackend.service;

import com.x4oc.blogbackend.dao.repository.ArticleRepository;
import com.x4oc.blogbackend.dao.repository.BlogRollItemRepository;
import com.x4oc.blogbackend.dao.repository.CommentaryRepository;
import com.x4oc.blogbackend.query.SumCommentariesEntry;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DashboardService {

    private final ArticleRepository articleRepository;
    private final CommentaryRepository commentaryRepository;
    private final CommentaryService commentaryService;
    private final BlogRollItemRepository blogRollItemRepository;

    public DashboardService(ArticleRepository articleRepository, CommentaryRepository commentaryRepository, CommentaryService commentaryService, BlogRollItemRepository blogRollItemRepository) {
        this.articleRepository = articleRepository;
        this.commentaryRepository = commentaryRepository;
        this.commentaryService = commentaryService;
        this.blogRollItemRepository = blogRollItemRepository;
    }

    @Transactional(readOnly = true)
    public Dashboard getDashboard(){
        return new Dashboard(
                articleRepository.count(),
                commentaryRepository.count() - 1,
                commentaryService.getSumCommentaries().getSums().entrySet().stream()
                        .sorted(Map.Entry.comparingByKey())
                        .map(e -> new SumCommentariesEntry(e.getKey().getId(), e.getValue().get()))
                        .collect(Collectors.toList()),
                blogRollItemRepository.count()
        );
    }
}
