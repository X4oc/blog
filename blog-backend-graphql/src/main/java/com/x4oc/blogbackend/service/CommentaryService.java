package com.x4oc.blogbackend.service;

import com.x4oc.blogbackend.dao.entity.Article;
import com.x4oc.blogbackend.dao.entity.Commentary;
import com.x4oc.blogbackend.dao.repository.ArticleRepository;
import com.x4oc.blogbackend.dao.repository.CommentaryRepository;
import com.x4oc.blogbackend.global.BlogPropertiesConfiguration;
import com.x4oc.blogbackend.mutation.CommentaryMutation;
import com.x4oc.blogbackend.query.SumCommentariesEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CommentaryService {
    private static final Logger logger = LoggerFactory.getLogger(CommentaryService.class);

    private final CommentaryRepository commentaryRepository;
    private final ArticleRepository articleRepository;
    private final SumCommentaries sumCommentaries;
    private final BlogPropertiesConfiguration blogPropertiesConfiguration;

    public CommentaryService(CommentaryRepository commentaryRepository, ArticleRepository articleRepository, SumCommentaries sumCommentaries, BlogPropertiesConfiguration blogPropertiesConfiguration) {
        this.commentaryRepository = commentaryRepository;
        this.articleRepository = articleRepository;
        this.sumCommentaries = sumCommentaries;
        this.blogPropertiesConfiguration = blogPropertiesConfiguration;
    }

    @Transactional
    public Commentary create(String author, String text, long articleId, long parentId){
        Optional<Article> articleOpt = articleRepository.findById(articleId);
        return articleOpt.map(article -> {
            Optional<Commentary> parentOpt = commentaryRepository.findById(parentId);
            Commentary parent = parentOpt.orElse(Commentary.root);
            Commentary savedCommentary = commentaryRepository.save(new Commentary(author, text, article, parent));
            sumCommentaries.addComment(article);
            return savedCommentary;
        }).orElseGet(() -> {
            logger.error("nonexistent article: "+articleId);
            return Commentary.invalidCommentary;
        });
    }

    @Transactional(readOnly = true)
    public List<Commentary> getAll(long count){
        return commentaryRepository.findAll().stream().limit(count).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<Commentary> getAllFromParent(long articleId, long parentId, long count){
        return commentaryRepository.findByArticle_IdAndParent_Id(articleId, (parentId > 0) ? parentId : Commentary.root.getId()).stream()
                .limit(count)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<Commentary> getLastNCommentaries(int limit){
        return commentaryRepository.findAll(PageRequest.of(0, limit, Sort.Direction.DESC, "date"))
                .getContent().stream()
                .filter(commentary -> commentary.getId() != Commentary.root.getId())
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<Commentary> getLastCommentaries(){
        return getLastNCommentaries(blogPropertiesConfiguration.getLastNCommentaries());
    }


    @Transactional(readOnly = true)
    public Optional<Commentary> get(long id){
        return commentaryRepository.findById(id);
    }


    @Transactional(readOnly = true)
    public long countByArticle(long articleId){
        return commentaryRepository.countByArticle_Id(articleId);
    }

    @Transactional(readOnly = true)
    public List<SumCommentariesEntry> getSumCommentaries(long last){
        return sumCommentaries.getSums().entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .limit(last)
                .map(e -> new SumCommentariesEntry(e.getKey().getId(), e.getValue().get()))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<Commentary> getAllCommentariesFromArticle(long articleId, int count){
        System.out.println("articleId: "+articleId+", count: "+count);
        return commentaryRepository.findByArticle_Id(articleId, PageRequest.of(0, count, Sort.Direction.ASC, "date"));
    }

    @Transactional
    public List<Commentary> updateCommentaries(List<CommentaryMutation.CommentaryInput> commentaries){
        return commentaries.stream()
                .map(c -> c.toCommentary(articleRepository.findById(c.getArticleId()).get(), commentaryRepository.findById(c.getParentId()).get()))
                .map(commentaryRepository::save)
                .collect(Collectors.toList());
    }

    SumCommentaries getSumCommentaries() {
        return sumCommentaries;
    }
}
