package com.x4oc.blogbackend.service;

import com.x4oc.blogbackend.dao.entity.Article;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Component
public final class SumCommentaries {
    private Map<Article, AtomicLong> sums;

    public SumCommentaries() {
        this.sums = new ConcurrentHashMap<>();
    }

    public Map<Article, AtomicLong> getSums() {
        return sums;
    }

    public long addComment(Article article){
        sums.putIfAbsent(article, new AtomicLong(0));
        return sums.get(article).incrementAndGet();
    }

    public long removeComment(Article article){
        return sums.get(article).decrementAndGet();
    }
}
