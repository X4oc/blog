package com.x4oc.blogbackend.service;

import com.x4oc.blogbackend.dao.entity.Article;
import com.x4oc.blogbackend.dao.repository.ArticleRepository;
import com.x4oc.blogbackend.dao.repository.CommentaryRepository;
import com.x4oc.blogbackend.global.BlogPropertiesConfiguration;
import com.x4oc.blogbackend.mutation.ArticleMutation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final CommentaryRepository commentaryRepository;

    public ArticleService(ArticleRepository articleRepository, CommentaryRepository commentaryRepository) {
        this.articleRepository = articleRepository;
        this.commentaryRepository = commentaryRepository;
    }

    public BlogPropertiesConfiguration blogPropertiesConfiguration;

    @Autowired
    public void setBlog(BlogPropertiesConfiguration blogPropertiesConfiguration) {
        this.blogPropertiesConfiguration = blogPropertiesConfiguration;
    }

    @Transactional(readOnly = true)
    public List<Article> getAllArticles() {
        return articleRepository.findAll();
    }

    @Transactional
    public Article create(String title, String category, String text) {
        return articleRepository.save(new Article(title, category, text));
    }

    @Transactional(readOnly = true)
    public List<Article> getLastNArticles(int limit) {
        return articleRepository.findAll(PageRequest.of(0, limit, Sort.Direction.DESC, "dateCreation"))
                .getContent();
    }

    @Transactional(readOnly = true)
    public List<Article> getLastArticles() {
        return getLastNArticles(blogPropertiesConfiguration.getLastNArticles());
    }

    @Transactional(readOnly = true)
    public Optional<Article> get(long id) {
        return articleRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public List<String> getAllCategories() {
        return articleRepository.findDistinctCategories();
    }

    @Transactional
    public List<Long> deleteArticles(List<ArticleMutation.ArticleInput> articles) {
        return articles.stream()
                .map(ArticleMutation.ArticleInput::toArticle)
                .peek(commentaryRepository::deleteAllByArticle)
                .peek(articleRepository::delete)
                .map(Article::getId)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<Article> updateArticles(List<ArticleMutation.ArticleInput> articles) {
        return articles.stream()
                .map(ArticleMutation.ArticleInput::toArticle)
                .map(Article::updateDateLastModified)
                .map(articleRepository::save)
                .collect(Collectors.toList());
    }
}