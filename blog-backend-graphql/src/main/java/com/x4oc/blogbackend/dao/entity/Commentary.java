package com.x4oc.blogbackend.dao.entity;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Commentary {
    public static final Commentary root = new Commentary(1, "NULL_AUTHOR", Instant.now(), "NULL_TEXT", null, null);
    public static final Commentary invalidCommentary = new Commentary(-1, "INVALID", Instant.now(), "INVALID", new Article("INVALID", "INVALID", "INVALID"), null);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String author;
    private Instant date;
    @Column(length = 300000)
    private String text;

    @ManyToOne
    private Article article;

    @OneToOne
    private Commentary parent;

    public Commentary() {}

    public Commentary(String author, String text, Article article, Commentary parent) {
        this.author = author;
        this.date = Instant.now();
        this.text = text;
        this.article = article;
        this.parent = parent;
    }

    public Commentary(long id, String author, Instant date, String text, Article article, Commentary parent) {
        this.id = id;
        this.author = author;
        this.date = date;
        this.text = text;
        this.article = article;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public long getArticleId(){
        return article.getId();
    }

    public long getParentId(){
        return parent.getId();
    }

    public String getAuthor() {
        return author;
    }

    public Instant getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public Article getArticle() {
        return article;
    }

    public Commentary getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "Commentary{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", date=" + date +
                ", parent=" + parent +
                '}';
    }
}
