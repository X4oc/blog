package com.x4oc.blogbackend.dao.entity;

import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
public class Article implements Comparable<Article>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String title;
    private OffsetDateTime dateCreation;
    private OffsetDateTime dateLastModified;
    private String category;
    @Column(length = 300000)
    private String text;

    public Article() {}

    public Article(String title, String category, String text) {
        this.title = title;
        this.dateCreation = OffsetDateTime.now();
        this.dateLastModified = OffsetDateTime.now();
        this.category = category;
        this.text = text;
    }

    public Article(long id, String title, OffsetDateTime dateCreation, OffsetDateTime dateLastModified, String category, String text) {
        this.id = id;
        this.title = title;
        this.dateCreation = dateCreation;
        this.dateLastModified = dateLastModified;
        this.category = category;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public OffsetDateTime getDateCreation() {
        return dateCreation;
    }

    public OffsetDateTime getDateLastModified() {
        return dateLastModified;
    }

    public String getCategory() {
        return category;
    }

    public String getText() {
        return text;
    }

    public Article updateDateLastModified() {
        this.dateLastModified = OffsetDateTime.now();
        return this;
    }

    @Override
    public int compareTo(@NotNull Article article) {
        return article.dateCreation.compareTo(dateCreation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        return id == article.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", dateCreation=" + dateCreation +
                ", dateLastModified=" + dateLastModified +
                ", category='" + category + '\'' +
                '}';
    }

}
