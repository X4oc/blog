package com.x4oc.blogbackend.dao.repository;

import com.x4oc.blogbackend.dao.entity.Article;
import com.x4oc.blogbackend.dao.entity.Commentary;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentaryRepository extends JpaRepository<Commentary, Long> {
    public List<Commentary> findByArticle_IdAndParent_Id(long articleId, long parentId);
    public long countByArticle_Id(long articleId);
    public List<Commentary> findByArticle_Id(long articleId, Pageable pageable);
    public void deleteAllByArticle(Article article);
}
