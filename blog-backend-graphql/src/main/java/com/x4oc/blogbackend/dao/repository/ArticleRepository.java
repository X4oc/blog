package com.x4oc.blogbackend.dao.repository;

import com.x4oc.blogbackend.dao.entity.Article;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
    @Query("SELECT DISTINCT a.category FROM Article a")
    public List<String> findDistinctCategories();
}
