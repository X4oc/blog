package com.x4oc.blogbackend.dao.repository;


import com.x4oc.blogbackend.dao.entity.BlogRollItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlogRollItemRepository extends JpaRepository<BlogRollItem, Long> {
    public List<BlogRollItem> findAllByCategory(String category);
}
