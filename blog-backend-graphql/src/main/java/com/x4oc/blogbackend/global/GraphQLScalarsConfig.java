package com.x4oc.blogbackend.global;

import com.coxautodev.graphql.tools.SchemaParserOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLScalarType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GraphQLScalarsConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper().registerModule(new JavaTimeModule());
    }

    @Bean
    public SchemaParserOptions schemaParserOptions() {
        return SchemaParserOptions.newOptions()
                .objectMapperProvider(fieldDefinition -> objectMapper()).build();
    }

    @Bean
    public GraphQLScalarType jsonType() {
        return ExtendedScalars.Json;
    }

    @Bean
    public GraphQLScalarType dateTimeType() {
        return ExtendedScalars.DateTime;
    }

    @Bean
    public GraphQLScalarType urlType() {
        return ExtendedScalars.Url;
    }
}
