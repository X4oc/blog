package com.x4oc.blogbackend.global;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.Objects;

@Configuration
@PropertySource("classpath:blog.properties")
@JsonIgnoreProperties("$$beanFactory")
public class BlogPropertiesConfiguration {

    private final String pathAdmin;
    private final String email;
    private final String twitter;
    private final String rss;
    private final String imgTitle;
    private final int lastNArticles;
    private final int lastNCommentaries;
    private final int maxBlogRollItems;
    private final String moderationCommentaryAuthor;
    private final String moderationCommentaryText;

    private Environment env;

    public BlogPropertiesConfiguration(Environment env) {
        this.env = env;
        this.pathAdmin = env.getProperty("pathAdmin");
        this.email = env.getProperty("email");
        this.twitter = env.getProperty("twitter");
        this.rss = env.getProperty("rss");
        this.imgTitle = env.getProperty("imgTitle");
        this.lastNArticles = Integer.parseInt(Objects.requireNonNull(env.getProperty("lastNArticles")));
        this.lastNCommentaries = Integer.parseInt(Objects.requireNonNull(env.getProperty("lastNCommentaries")));
        this.maxBlogRollItems = Integer.parseInt(Objects.requireNonNull(env.getProperty("maxBlogRollItems")));
        this.moderationCommentaryAuthor = env.getProperty("moderationCommentaryAuthor");
        this.moderationCommentaryText = env.getProperty("moderationCommentaryText");
    }

    public String getPathAdmin() {
        return pathAdmin;
    }

    public String getEmail() {
        return email;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getRss() {
        return rss;
    }

    public String getImgTitle() {
        return imgTitle;
    }

    public int getLastNArticles() {
        return lastNArticles;
    }

    public int getLastNCommentaries() {
        return lastNCommentaries;
    }

    public int getMaxBlogRollItems() {
        return maxBlogRollItems;
    }

    public String getModerationCommentaryAuthor() {
        return moderationCommentaryAuthor;
    }

    public String getModerationCommentaryText() {
        return moderationCommentaryText;
    }

    @Override
    public String toString() {
        return "BlogPropertiesConfiguration{" +
                "pathAdmin='" + pathAdmin + '\'' +
                ", email='" + email + '\'' +
                ", twitter='" + twitter + '\'' +
                ", rss='" + rss + '\'' +
                ", imgTitle='" + imgTitle + '\'' +
                ", lastNArticles=" + lastNArticles +
                ", lastNCommentaries=" + lastNCommentaries +
                ", maxBlogRollItems=" + maxBlogRollItems +
                ", moderationCommentaryAuthor='" + moderationCommentaryAuthor + '\'' +
                ", moderationCommentaryText='" + moderationCommentaryText + '\'' +
                '}';
    }
}
