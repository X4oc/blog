package com.x4oc.blogbackend.global;

import org.springframework.stereotype.Component;

@Component
public class BlogProperties {

    private String pathAdmin;
    private String email;
    private String twitter;
    private String rss;
    private String imgTitle;
    private int lastNArticles;
    private int lastNCommentaries;
    private int maxBlogRollItems;
    private String moderationCommentaryAuthor;
    private String moderationCommentaryText;

    public BlogProperties() {}

    public BlogProperties(BlogPropertiesConfiguration config) {
        this.pathAdmin = config.getPathAdmin();
        this.email = config.getEmail();
        this.twitter = config.getTwitter();
        this.rss = config.getRss();
        this.imgTitle = config.getImgTitle();
        this.lastNArticles = config.getLastNArticles();
        this.lastNCommentaries = config.getLastNCommentaries();
        this.maxBlogRollItems = config.getMaxBlogRollItems();
        this.moderationCommentaryAuthor = config.getModerationCommentaryAuthor();
        this.moderationCommentaryText = config.getModerationCommentaryText();
    }

    public String getPathAdmin() {
        return pathAdmin;
    }

    public String getEmail() {
        return email;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getRss() {
        return rss;
    }

    public String getImgTitle() {
        return imgTitle;
    }

    public int getLastNArticles() {
        return lastNArticles;
    }

    public int getLastNCommentaries() {
        return lastNCommentaries;
    }

    public int getMaxBlogRollItems() {
        return maxBlogRollItems;
    }

    public String getModerationCommentaryAuthor() {
        return moderationCommentaryAuthor;
    }

    public String getModerationCommentaryText() {
        return moderationCommentaryText;
    }

    @Override
    public String toString() {
        return "BlogProperties{" +
                "pathAdmin='" + pathAdmin + '\'' +
                ", email='" + email + '\'' +
                ", twitter='" + twitter + '\'' +
                ", rss='" + rss + '\'' +
                ", imgTitle='" + imgTitle + '\'' +
                ", lastNArticles=" + lastNArticles +
                ", lastNCommentaries=" + lastNCommentaries +
                ", maxBlogRollItems=" + maxBlogRollItems +
                ", moderationCommentaryAuthor='" + moderationCommentaryAuthor + '\'' +
                ", moderationCommentaryText='" + moderationCommentaryText + '\'' +
                '}';
    }
}
