package com.x4oc.blogbackend.global;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import com.x4oc.blogbackend.dao.entity.*;
import com.x4oc.blogbackend.dao.repository.CommentaryRepository;
import com.x4oc.blogbackend.service.ArticleService;
import com.x4oc.blogbackend.service.BlogRollItemService;
import com.x4oc.blogbackend.service.CommentaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;

import org.springframework.stereotype.Component;


import java.util.Random;
import java.util.stream.IntStream;

@Component
public class Initialization implements CommandLineRunner {

    private static final Lorem lorem = LoremIpsum.getInstance();

    private ArticleService articleService;
    private BlogRollItemService blogRollItemService;
    private CommentaryService commentaryService;

    private CommentaryRepository commentaryRepository;

    @Value("${fill.db.random}")
    private String fillDbRandom;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setBlogRollItemService(BlogRollItemService blogRollItemService) {
        this.blogRollItemService = blogRollItemService;
    }

    @Autowired
    public void setCommentaryService(CommentaryService commentaryService) {
        this.commentaryService = commentaryService;
    }

    @Autowired
    public void setCommentaryRepository(CommentaryRepository commentaryRepository) {
        this.commentaryRepository = commentaryRepository;
    }

    @Override
    public void run(String... args) {
        Commentary root = commentaryRepository.save(Commentary.root);
        if(Boolean.parseBoolean(fillDbRandom)){
            fillDB(root);
        }
    }

    private void fillDB(Commentary root){
        Article firstArticle = articleService.create("titre1", "category1", "text1");
        Commentary firstCom = commentaryService.create("A1", "T1", firstArticle.getId(), root.getId());
        Commentary secondCom = commentaryService.create("A2", "T2", firstArticle.getId(), root.getId());
        commentaryService.create("A21", "T21", firstArticle.getId(), root.getId());
        commentaryService.create("A22", "T22", firstArticle.getId(), root.getId());
        commentaryService.create("A23", "T23", firstArticle.getId(), root.getId());
        commentaryService.create("A24", "T24", firstArticle.getId(), root.getId());
        commentaryService.create("A3", "T3", firstArticle.getId(), firstCom.getId());
        commentaryService.create("A31", "T31", firstArticle.getId(), secondCom.getId());
        commentaryService.create("A32", "T32", firstArticle.getId(), firstCom.getId());
        commentaryService.create("A33", "T33", firstArticle.getId(), secondCom.getId());
        commentaryService.create("A34", "T34", firstArticle.getId(), firstCom.getId());

        IntStream.rangeClosed(0, 9).forEach(i -> {

            blogRollItemService.create(lorem.getTitle(1), "http://localhost:3000/about","friend blogs");
            blogRollItemService.create(lorem.getTitle(1), "http://localhost:3000","misc");

            Article articleIt = articleService.create(lorem.getTitle(4), "it", lorem.getParagraphs(3, 5));
            Article articleInfo = articleService.create(lorem.getTitle(4), "info", lorem.getParagraphs(1, 2));

            IntStream.rangeClosed(0, new Random().ints(1, 10).findFirst().getAsInt())
                    .forEach(j -> {
                        commentaryService.create(lorem.getName(), lorem.getWords(1, 10), articleIt.getId(), root.getId());
                        Commentary commentaryInfo = commentaryService.create(lorem.getName(), lorem.getWords(1, 10), articleInfo.getId(), root.getId());
                        IntStream.rangeClosed(0, new Random().ints(1, 5).findFirst().getAsInt())
                                .forEach(k -> commentaryService.create(lorem.getName(), lorem.getWords(1, 10), articleInfo.getId(), commentaryInfo.getId()));
                    });
        });
    }
}
