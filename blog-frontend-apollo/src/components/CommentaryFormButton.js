import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import {Button} from "@material-ui/core";

function CommentaryFormButton(props) {
    const commentary = props.commentary;
    const CREATE_COMMENTARY = gql`
        mutation CreateCommentary($author: String, $text: String!, $articleId: ID!, $parentId: ID!){
            createCommentary(author: $author, text: $text, articleId: $articleId, parentId: $parentId){id, author, date, text, articleId, parentId}
        }
    `;

    function createCommentary(options){
        update(options);
    }
    const [update] = useMutation(CREATE_COMMENTARY, []);
    return (
        <div>
            <Button onClick={() => createCommentary({variables: {
                author: commentary.author,
                text: commentary.text,
                articleId: commentary.articleId,
                parentId: commentary.parentId
                }})}>
                Create
            </Button>
        </div>
    );
}

export default CommentaryFormButton;