import React from 'react';

function About() {
    return (
        <div className='about'>
            <h1>About me</h1>
            <p>X4oc, JavaEE developer, currently developing one of the ten projects described {}
                <a href="https://purelyfunctional.tv/guide/programming-projects-resume/">here</a>.</p>
        </div>
    );
}

export default About;