import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import Button from "@material-ui/core/Button";

function AddBlogRollButton(props) {
    const blogRollItem = props.blogRoll;
    const CREATE_BLOG_ROLL_ITEM = gql`
        mutation CreateBlogRollItem($title: String!, $category: String!, $url: String){
            createBlogRollItem(title: $title, category: $category, url: $url){id, title, category, url}
        }
    `;

    const [add] = useMutation(CREATE_BLOG_ROLL_ITEM, []);

    async function handleSave(options) {
        let bri = await add(options);
        props.handleAdd(bri.data.createBlogRollItem);
    }
    return (
        <div>
            <Button onClick={() => handleSave({variables: {title: blogRollItem.title, category: blogRollItem.category, url: blogRollItem.url}})} color="primary">
                Add
            </Button>
        </div>
    );
}

export default AddBlogRollButton;