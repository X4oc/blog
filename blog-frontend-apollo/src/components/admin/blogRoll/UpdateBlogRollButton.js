import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import Button from "@material-ui/core/Button";

function UpdateBlogRollButton(props) {
    const UPDATE_BLOG_ROLL_ITEMS = gql`
        mutation UpdateBlogRollItems($blogRollItems: [BlogRollItemInput]){
            updateBlogRollItems(blogRollItems: $blogRollItems){id, title, url, category}
        }
    `;

    const [update] = useMutation(UPDATE_BLOG_ROLL_ITEMS, []);

    async function updateBlogRollItems() {
        const blogRollItemsToUpdate = props.handleUpdate();
        const properBlogRollItemsToUpdate = blogRollItemsToUpdate.map(({__typename, ...rest}) => rest)
        await update({variables: {blogRollItems: properBlogRollItemsToUpdate}});
    }

    return (
        <div>
            <Button onClick={() => updateBlogRollItems()} color="primary">
                Update
            </Button>
        </div>
    );
}

export default UpdateBlogRollButton;