import React from 'react';
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import TableBlogRollAdmin from "./TableBlogRollAdmin";

function BlogRollAdmin() {
    const GET_BLOG_ROLL_ITEMS = gql`
        query GetBlogRollItems{
            getBlogRollItems{id, title, url, category}
        }
    `;

    const { loading, error, data } = useQuery(GET_BLOG_ROLL_ITEMS);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error!</div>;

    const blogRollItems = data.getBlogRollItems;

    return (
        <div>
            <TableBlogRollAdmin blogRollItems={blogRollItems} />
        </div>
    )

}

export default BlogRollAdmin;