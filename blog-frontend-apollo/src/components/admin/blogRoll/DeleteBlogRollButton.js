import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import Button from "@material-ui/core/Button";

function DeleteBlogRollButton(props) {
    const DELETE_BLOG_ROLL_ITEMS = gql`
        mutation DeleteBlogRollItems($blogRollItems: [BlogRollItemInput]){
            deleteBlogRollItems(blogRollItems: $blogRollItems)
        }
    `;

    const [del] = useMutation(DELETE_BLOG_ROLL_ITEMS, []);

    async function handleRemove() {
        const blogRollItemsToDelete = props.handleDelete();
        const properBlogRollItemsToDelete = blogRollItemsToDelete.map(({__typename, ...rest}) => rest)
        await del({variables: {blogRollItems: properBlogRollItemsToDelete}});
    }

    return (
        <div>
            <Button onClick={() => handleRemove()} color="primary">
                Delete
            </Button>
        </div>
    );
}

export default DeleteBlogRollButton;