import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import EnhancedTableBlogRoll from "./EnhancedTableBlogRoll";

function TableBlogRollAdmin(props) {
    const columns = React.useMemo(
        () => [
            {
                Header: 'ID',
                accessor: 'id',
            },
            {
                Header: 'Title',
                accessor: 'title',
            },
            {
                Header: 'URL',
                accessor: 'url',
            },
            {
                Header: 'Category',
                accessor: 'category',
            },
        ],
        []
    )

    const [data, setData] = React.useState(React.useMemo(() => props.blogRollItems, [props.blogRollItems]))
    const [skipPageReset, setSkipPageReset] = React.useState(false)

    const updateMyData = (rowIndex, columnId, value) => {
        setSkipPageReset(true)
        setData(old =>
            old.map((row, index) => {
                if (index === rowIndex) {
                    return {
                        ...old[rowIndex],
                        [columnId]: value,
                    };
                }
                return row
            })
        )
    }

    return (
        <div>
            <CssBaseline />
            <EnhancedTableBlogRoll
                columns={columns}
                data={data}
                setData={setData}
                updateMyData={updateMyData}
                skipPageReset={skipPageReset}
            />
        </div>
    )
}

export default TableBlogRollAdmin;