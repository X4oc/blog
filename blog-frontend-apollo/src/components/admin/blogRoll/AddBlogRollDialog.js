import React, { useState } from 'react'

import AddIcon from '@material-ui/icons/Add'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import Switch from '@material-ui/core/Switch'
import TextField from '@material-ui/core/TextField'
import Tooltip from '@material-ui/core/Tooltip'
import AddBlogRollButton from "./AddBlogRollButton";

const initialBlogRollItem = {
  title: '',
  url: '',
  category: '',
  subRows: undefined,
}

const AddBlogRollDialog = props => {

  const [blogRoll, setBlogRoll] = useState(initialBlogRollItem)

  const { addBlogRollHandler } = props
  const [open, setOpen] = React.useState(false)

  const [switchState, setSwitchState] = React.useState({
    addMultiple: false,
  })

  const handleSwitchChange = name => event => {
    setSwitchState({ ...switchState, [name]: event.target.checked })
  }

  const resetSwitch = () => {
    setSwitchState({ addMultiple: false })
  }

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
    resetSwitch()
  }

  const handleAdd = bri => {
    addBlogRollHandler(bri)
    setBlogRoll(initialBlogRollItem)
    switchState.addMultiple ? setOpen(true) : setOpen(false)
  }

  const handleChange = name => ({ target: { value } }) => {
    setBlogRoll({ ...blogRoll, [name]: value })
  }

  return (
    <div>
      <Tooltip title="Add BlogRollItem">
        <IconButton aria-label="add" onClick={handleClickOpen}>
          <AddIcon />
        </IconButton>
      </Tooltip>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Add BlogRollItem</DialogTitle>
        <DialogContent>
          <DialogContentText>Add article</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            label="Title"
            type="text"
            fullWidth
            value={blogRoll.title}
            onChange={handleChange('title')}
          />
          <TextField
            margin="dense"
            label="URL"
            type="text"
            fullWidth
            value={blogRoll.url}
            onChange={handleChange('url')}
          />
          <TextField
            margin="dense"
            label="Category"
            type="text"
            fullWidth
            value={blogRoll.category}
            onChange={handleChange('category')}
          />
        </DialogContent>
        <DialogActions>
          <Tooltip title="Add multiple">
            <Switch
              checked={switchState.addMultiple}
              onChange={handleSwitchChange('addMultiple')}
              value="addMultiple"
              inputProps={{ 'aria-label': 'secondary checkbox' }}
            />
          </Tooltip>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <AddBlogRollButton blogRoll={blogRoll} handleAdd={handleAdd} />
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default AddBlogRollDialog
