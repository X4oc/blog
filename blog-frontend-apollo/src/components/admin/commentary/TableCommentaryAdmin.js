import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline'
import EnhancedTableCommentary from "./EnhancedTableCommentary";

function TableCommentaryAdmin(props) {
    const blog = props.blog;
    const columns = React.useMemo(
        () => [
            {
                Header: 'ID',
                accessor: 'id',
            },
            {
                Header: 'Author',
                accessor: 'author',
            },
            {
                Header: 'Date',
                accessor: 'date',
            },
            {
                Header: 'Text',
                accessor: 'text',
            },
            {
                Header: 'Article ID',
                accessor: 'articleId',
            },
            {
                Header: 'Parent ID',
                accessor: 'parentId',
            },
        ],
        []
    )

    const [data, setData] = React.useState(React.useMemo(() => props.commentaries, [props.commentaries]))
    const [skipPageReset, setSkipPageReset] = React.useState(false)

    const updateMyData = (rowIndex, columnId, value) => {
        setSkipPageReset(true)
        setData(old =>
            old.map((row, index) => {
                if (index === rowIndex) {
                    return {
                        ...old[rowIndex],
                        [columnId]: value,
                    };
                }
                return row
            })
        )
    }

    return (
        <div>
            <CssBaseline />
            <EnhancedTableCommentary
                columns={columns}
                data={data}
                setData={setData}
                updateMyData={updateMyData}
                skipPageReset={skipPageReset}
                blog={blog}
            />
        </div>
    )
}

export default TableCommentaryAdmin;