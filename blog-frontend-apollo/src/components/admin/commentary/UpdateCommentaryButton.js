import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import Button from "@material-ui/core/Button";

function UpdateCommentaryButton(props) {
    const UPDATE_COMMENTARIES = gql`
        mutation UpdateCommentaries($commentaries: [CommentaryInput]){
            updateCommentaries(commentaries: $commentaries){id, author, date, text, articleId, parentId}
        }
    `;

    const [update] = useMutation(UPDATE_COMMENTARIES, []);

    async function updateCommentaries() {
        const commentariesToUpdate = props.handleUpdate();
        const properCommentariesToUpdate = commentariesToUpdate.map(({__typename, ...rest}) => rest)
        await update({variables: {commentaries: properCommentariesToUpdate}});
    }

    return (
        <div>
            <Button onClick={() => updateCommentaries()} color="primary">
                {props.label}
            </Button>
        </div>
    );
}

export default UpdateCommentaryButton;