import React from 'react';
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import TableCommentaryAdmin from "./TableCommentaryAdmin";

function CommentaryAdmin(props) {
    const blog = props.blog;

    const GET_ALL_COMMENTARIES = gql`
        query GetLastCommentaries{
            getLastCommentaries{id, author, date, text, articleId, parentId}
        }
    `;

    const { loading, error, data } = useQuery(GET_ALL_COMMENTARIES);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error!</div>;

    const commentaries = data.getLastCommentaries;

    return (
        <div>
            <TableCommentaryAdmin commentaries={commentaries} blog={blog}/>
        </div>
    )
}

export default CommentaryAdmin;