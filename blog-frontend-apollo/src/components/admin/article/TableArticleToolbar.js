import React from 'react'

import AddArticleDialog from './AddArticleDialog'
import clsx from 'clsx'
import GlobalFilter from "../../react-table-custom/GlobalFilter";
import { lighten, makeStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import DeleteArticleButton from "./DeleteArticleButton";
import UpdateArticleButton from "./UpdateArticleButton";

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}))

const TableArticleToolbar = props => {
  const classes = useToolbarStyles()
  const {
    numSelected,
    addArticleHandler,
    deleteArticleHandler,
    updateArticleHandler,
    preGlobalFilteredRows,
    setGlobalFilter,
    globalFilter,
  } = props
  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <AddArticleDialog addArticleHandler={addArticleHandler} />
      {numSelected > 0 ? (
        <Typography
          className={classes.title}
          color="inherit"
          variant="subtitle1"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle">
        </Typography>
      )}

      {numSelected > 0 ? (
        <div>
          <Tooltip title="Update">
            <div>
              <UpdateArticleButton aria-label="update" handleUpdate={updateArticleHandler} />
            </div>
          </Tooltip>
          <Tooltip title="Delete">
            <div>
              <DeleteArticleButton aria-label="delete" handleDelete={deleteArticleHandler} />
            </div>
          </Tooltip>
        </div>
      ) : (
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      )}
    </Toolbar>
  )
}

export default TableArticleToolbar
