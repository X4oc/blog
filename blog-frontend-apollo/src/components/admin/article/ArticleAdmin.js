import React from 'react';
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import TableArticleAdmin from "./TableArticleAdmin";

function ArticleAdmin() {
    const GET_ALL_ARTICLES = gql`
        query GetLastArticles{
            getAllArticles{id, title, dateCreation, dateLastModified, category, text}
        }
    `;

    const { loading, error, data } = useQuery(GET_ALL_ARTICLES);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error!</div>;

    const articles = data.getAllArticles;

    return (
        <div>
            <TableArticleAdmin articles={articles} />
        </div>
    )
}

export default ArticleAdmin;