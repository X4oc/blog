import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import Button from "@material-ui/core/Button";

function DeleteArticleButton(props) {
    const DELETE_ARTICLES = gql`
        mutation DeleteArticle($articles: [ArticleInput]){
            deleteArticles(articles: $articles)
        }
    `;

    const [del] = useMutation(DELETE_ARTICLES, []);

    async function handleRemove() {
        const articlesToDelete = props.handleDelete();
        const properArticlesToDelete = articlesToDelete.map(({__typename, ...rest}) => rest)
        await del({variables: {articles: properArticlesToDelete}});
    }

    return (
        <div>
            <Button onClick={() => handleRemove()} color="primary">
                Delete
            </Button>
        </div>
    );
}

export default DeleteArticleButton;