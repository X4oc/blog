import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import Button from "@material-ui/core/Button";

function UpdateArticleButton(props) {
    const UPDATE_ARTICLES = gql`
        mutation UpdateArticle($articles: [ArticleInput]){
            updateArticles(articles: $articles){id, title, dateCreation, dateLastModified, category, text}
        }
    `;

    const [update] = useMutation(UPDATE_ARTICLES, []);

    async function updateArticles() {
        const articlesToUpdate = props.handleUpdate();
        const properArticlesToUpdate = articlesToUpdate.map(({__typename, ...rest}) => rest)
        await update({variables: {articles: properArticlesToUpdate}});
    }

    return (
        <div>
            <Button onClick={() => updateArticles()} color="primary">
                Update
            </Button>
        </div>
    );
}

export default UpdateArticleButton;