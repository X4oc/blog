import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline'
import EnhancedTableArticle from "./EnhancedTableArticle";

function TableArticleAdmin(props) {
    const columns = React.useMemo(
        () => [
            {
                Header: 'ID',
                accessor: 'id',
            },
            {
                Header: 'Title',
                accessor: 'title',
            },
            {
                Header: 'Date Creation',
                accessor: 'dateCreation',
            },
            {
                Header: 'Date Last Modified',
                accessor: 'dateLastModified',
            },
            {
                Header: 'Category',
                accessor: 'category',
            },
            {
                Header: 'Text',
                accessor: 'text',
            },
        ],
        []
    )

    const [data, setData] = React.useState(React.useMemo(() => props.articles, [props.articles]))
    const [skipPageReset, setSkipPageReset] = React.useState(false)

    const updateMyData = (rowIndex, columnId, value) => {
        setSkipPageReset(true)
        setData(old =>
            old.map((row, index) => {
                if (index === rowIndex) {
                    return {
                        ...old[rowIndex],
                        [columnId]: value,
                    };
                }
                return row
            })
        )
    }

    return (
        <div>
            <CssBaseline />
            <EnhancedTableArticle
                columns={columns}
                data={data}
                setData={setData}
                updateMyData={updateMyData}
                skipPageReset={skipPageReset}
            />
        </div>
    )
}

export default TableArticleAdmin;