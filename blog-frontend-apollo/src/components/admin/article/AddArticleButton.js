import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import Button from "@material-ui/core/Button";

function AddArticleButton(props) {
    const article = props.article;
    const CREATE_ARTICLE = gql`
        mutation CreateArticle($title: String!, $category: String!, $text: String){
            createArticle(title: $title, category: $category, text: $text){id, title, dateCreation, dateLastModified, category, text}
        }
    `;

    const [add] = useMutation(CREATE_ARTICLE, []);

    async function handleSave(options) {
        let art = await add(options);
        props.handleAdd(art.data.createArticle);
    }
    return (
        <div>
            <Button onClick={() => handleSave({variables: {title: article.title, category: article.category, text: article.text}})} color="primary">
                Add
            </Button>
        </div>
    );
}

export default AddArticleButton;