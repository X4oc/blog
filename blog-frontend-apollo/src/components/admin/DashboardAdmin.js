import React from 'react';
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import {List} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";

function DashboardAdmin() {
    const GET_DASHBOARD = gql`
        query GetDashboard {
            getDashboard{
                sumArticles,
                sumCommentaries,
                sumCommentariesPerArticle {
                    articleId, sum
                },
                sumBlogRollEntries}
        }`;

    const { loading, error, data } = useQuery(GET_DASHBOARD);

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error!</div>;
    const dashboard = data.getDashboard;

    return (
        <div>
            <h1>Stats</h1>
            <List>
                <ListItem key="sumArticles">Articles: {dashboard.sumArticles}</ListItem>
                <ListItem key="sumCommentaries">Commentaries: {dashboard.sumCommentaries}</ListItem>
                <ListItem key="sumBlogRollEntries">Blog Roll Entries: {dashboard.sumBlogRollEntries}</ListItem>
                <List>
                    {dashboard.sumCommentariesPerArticle.map(entry =>
                    <ListItem key={entry.articleId}>Article ID: {entry.articleId}, # Commentaries: {entry.sum}</ListItem>
                    )}
                </List>
            </List>
        </div>
    );
}

export default DashboardAdmin;