import React from 'react';
import UpdateSettingsForm from "./UpdateSettingsForm";

function SettingsAdmin(props) {
    const blog = props.blog;
    const {__typename, ...settings} = blog;
    return (
        <div>
            <UpdateSettingsForm settings={settings}/>
        </div>
    )
}

export default SettingsAdmin;