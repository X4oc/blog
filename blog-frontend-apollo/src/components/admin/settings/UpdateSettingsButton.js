import React from 'react';
import {gql} from "apollo-boost";
import {useMutation} from "@apollo/client";
import {Button} from "@material-ui/core";

function UpdateSettingsButton(props) {
    const UPDATE_SETTINGS = gql`
        mutation UpdateSettings($settings: BlogPropertiesInput){
            setBlogProperties(input: $settings){pathAdmin, email, rss, twitter, imgTitle,
                lastNArticles, lastNCommentaries, maxBlogRollItems, 
                moderationCommentaryAuthor, moderationCommentaryText}
        }
    `;

    function updateSettings(options){
        update(options);
        //window.location.reload();
    }
    const [update] = useMutation(UPDATE_SETTINGS, []);

    return (
        <div>
            <Button onClick={() => updateSettings({variables: {settings: props.settings}})}>
                Update
            </Button>
        </div>
    );
}

export default UpdateSettingsButton;