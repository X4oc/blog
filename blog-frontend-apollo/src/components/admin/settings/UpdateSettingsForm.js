import React from 'react';
import TextField from "@material-ui/core/TextField";
import {makeStyles} from "@material-ui/core/styles";
import UpdateSettingsButton from "./UpdateSettingsButton";

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

function UpdateSettingsForm(props) {
    const initialSettings = props.settings;
    const classes = useStyles();
    const [pathAdmin, setPathAdmin] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [twitter, setTwitter] = React.useState("");
    const [rss, setRss] = React.useState("");
    const [imgTitle, setImgTitle] = React.useState("");
    const [lastNArticles, setLastNArticles] = React.useState("");
    const [lastNCommentaries, setLastNCommentaries] = React.useState("");
    const [maxBlogRollItems, setMaxBlogRollItems] = React.useState("");
    const [moderationCommentaryAuthor, setModerationCommentaryAuthor] = React.useState("");
    const [moderationCommentaryText, setModerationCommentaryText] = React.useState("");

    return (
        <form className={classes.root} noValidate>
            <div>
                <TextField
                    label="Path Admin"
                    id="filled-size-small"
                    defaultValue={initialSettings.pathAdmin}
                    variant="filled"
                    size="small"
                    onChange={(e) => setPathAdmin(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Email"
                    id="filled-size-small"
                    defaultValue={initialSettings.email}
                    variant="filled"
                    size="small"
                    onChange={(e) => setEmail(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="RSS"
                    id="filled-size-small"
                    defaultValue={initialSettings.rss}
                    variant="filled"
                    size="small"
                    onChange={(e) => setRss(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Twitter"
                    id="filled-size-small"
                    defaultValue={initialSettings.twitter}
                    variant="filled"
                    size="small"
                    onChange={(e) => setTwitter(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Image title"
                    id="filled-size-small"
                    defaultValue={initialSettings.imgTitle}
                    variant="filled"
                    size="small"
                    onChange={(e) => setImgTitle(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Last N Articles"
                    type="number"
                    id="filled-size-small"
                    defaultValue={initialSettings.lastNArticles}
                    variant="filled"
                    size="small"
                    onChange={(e) => setLastNArticles(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Last N Commentaries"
                    type="number"
                    id="filled-size-small"
                    defaultValue={initialSettings.lastNCommentaries}
                    variant="filled"
                    size="small"
                    onChange={(e) => setLastNCommentaries(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Max BlogRoll Items"
                    type="number"
                    id="filled-size-small"
                    defaultValue={initialSettings.maxBlogRollItems}
                    variant="filled"
                    size="small"
                    onChange={(e) => setMaxBlogRollItems(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Moderation Commentary Author"
                    id="filled-size-small"
                    defaultValue={initialSettings.moderationCommentaryAuthor}
                    variant="filled"
                    size="small"
                    onChange={(e) => setModerationCommentaryAuthor(e.target.value)}
                />
            </div>
            <div>
                <TextField
                    label="Moderation Commentary Text"
                    id="filled-size-small"
                    defaultValue={initialSettings.moderationCommentaryText}
                    variant="filled"
                    size="small"
                    onChange={(e) => setModerationCommentaryText(e.target.value)}
                />
            </div>
            <UpdateSettingsButton settings={
                {pathAdmin: pathAdmin || initialSettings.pathAdmin,
                email: email || initialSettings.email,
                rss: rss || initialSettings.rss,
                twitter: twitter || initialSettings.twitter,
                imgTitle: imgTitle || initialSettings.imgTitle,
                lastNArticles: lastNArticles || initialSettings.lastNArticles,
                lastNCommentaries: lastNCommentaries || initialSettings.lastNCommentaries,
                maxBlogRollItems: maxBlogRollItems || initialSettings.maxBlogRollItems,
                moderationCommentaryAuthor: moderationCommentaryAuthor || initialSettings.moderationCommentaryAuthor,
                moderationCommentaryText: moderationCommentaryText || initialSettings.moderationCommentaryText
            }}/>
            <p align="center">Reload after update</p>

        </form>
    );
}

export default UpdateSettingsForm;