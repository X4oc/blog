import React from 'react';
import {NavLink} from 'react-router-dom';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

function NavigationAdmin(props) {
    const useStyles = makeStyles({
        root: {
            flexGrow: 1,
        },
    });

    const classes = useStyles();
    const [value, setValue] = React.useState(false);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const blog = props.blog;
    const pathAdmin = '/' + blog.pathAdmin;

    return (
        <Paper className={classes.root}>
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                centered
            >
                <Tab label="Dashboard" component={NavLink} to={pathAdmin}/>
                <Tab label="Articles" component={NavLink} to={pathAdmin + '/articles'}/>
                <Tab label="Blog Roll" component={NavLink} to={pathAdmin + '/blogRoll'}/>
                <Tab label="Commentaries" component={NavLink} to={pathAdmin + '/commentaries'}/>
                <Tab label="Settings" component={NavLink} to={pathAdmin + '/settings'}/>
            </Tabs>
        </Paper>
    );
}

export default NavigationAdmin;