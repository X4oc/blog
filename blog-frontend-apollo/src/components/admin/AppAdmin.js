import React from 'react';
import NavigationAdmin from "./NavigationAdmin";
import MainAdmin from "./MainAdmin";

function AppAdmin(props) {
    const blog = props.blog;
    return (
        <div>
            <NavigationAdmin blog={blog}/>
            <MainAdmin blog={blog}/>
        </div>
    );
}

export default AppAdmin;