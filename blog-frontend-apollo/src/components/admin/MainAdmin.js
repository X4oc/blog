import React from 'react';
import {Route, Switch} from "react-router-dom";
import BlogRollAdmin from "./blogRoll/BlogRollAdmin";
import ArticleAdmin from "./article/ArticleAdmin";
import SettingsAdmin from "./settings/SettingsAdmin";
import DashboardAdmin from "./DashboardAdmin";
import CommentaryAdmin from "./commentary/CommentaryAdmin";

function MainAdmin(props) {
    const blog = props.blog;
    const pathAdmin = '/' + blog.pathAdmin;

    return (
        <div>
            <Switch>
                <Route exact path={pathAdmin} component={DashboardAdmin}/>
                <Route exact path={pathAdmin + '/articles'} component={ArticleAdmin} />
                <Route exact path={pathAdmin + '/blogRoll'} component={BlogRollAdmin} />
                <Route path={pathAdmin + '/commentaries'} render={(props) => (
                    <CommentaryAdmin {...props} blog={blog} />
                )}/>
                <Route path={pathAdmin + '/settings'} render={(props) => (
                    <SettingsAdmin {...props} blog={blog} />
                )}/>
            </Switch>
        </div>
    );
}

export default MainAdmin;