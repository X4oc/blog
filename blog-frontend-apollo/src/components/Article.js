import React from 'react';
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import CommentaryTree from "./CommentaryTree";
import {DateTime} from "luxon";

function Article(props) {
    const GET_ARTICLE = gql`
    query GetArticle($articleId: ID){
        getArticle(id: $articleId){id, title, dateCreation, dateLastModified, text, category}
        countCommentariesByArticle(articleId: $articleId)
        getAllCommentariesFromArticle(articleId: $articleId){id, author, date, text, articleId, parentId}
    }
    `;

    const { loading, error, data } = useQuery(GET_ARTICLE, {
        variables: { articleId: props.match.params.articleId},
    });

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error!</div>;

    const art = data.getArticle;
    const nbCom = data.countCommentariesByArticle;
    const coms = data.getAllCommentariesFromArticle;

    function formatDate(date){
        return DateTime.fromISO(date).toHTTP();
    }

    return (
        <div>
            <h1>{art.title}</h1>
                <p>{formatDate(art.dateCreation)}, {nbCom} commentaries, {art.category}</p>
                <p>{art.text}</p>
                <p>Last modified: {formatDate(art.dateLastModified)}</p>
                <CommentaryTree commentaries={coms} articleId={art.id}/>

        </div>
    );
}

export default Article;