import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import CommentaryFormButton from "./CommentaryFormButton";
import {Typography} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    title: {
        flex: '1 1 100%',
        marginLeft: 10
    },
}));

function CommentaryForm(props) {
    const classes = useStyles();
    const articleId = props.articleId;
    const parentId = props.parentId;
    const [author, setAuthor] = React.useState("");
    const [text, setText] = React.useState("");

    return (
        <div>
            <Typography className={classes.title} variant="h6" >Add comment</Typography>
            <form className={classes.root} noValidate id="commentary-form">
                <div>
                    <TextField
                        label="Author"
                        id="filled-size-small"
                        variant="filled"
                        size="small"
                        onChange={(e) => setAuthor(e.target.value)}
                    />
                </div>
                <div>
                    <TextField
                        label="Text"
                        id="filled-size-small"
                        variant="filled"
                        size="small"
                        onChange={(e) => setText(e.target.value)}
                    />
                </div>

                <CommentaryFormButton commentary={
                    {author: author,
                    text: text,
                    articleId: articleId,
                    parentId: parentId}
                } />
            </form>
        </div>
    );
}

export default CommentaryForm;