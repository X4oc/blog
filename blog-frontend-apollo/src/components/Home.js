import React from 'react';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import ArticleInsight from "./ArticleInsight";

const GET_ARTICLES_AND_COMMENTARIES = gql`
    query GetArticles($last: Int) {
        getLastNArticles(limit: $last) {
            id, title, dateCreation, text, category
        }
        getSumCommentaries(last: $last){
            articleId, sum
        }
    }`;


function Home(props) {
    const { loading, error, data } = useQuery(GET_ARTICLES_AND_COMMENTARIES, {
        variables: { last: props.blog.lastNArticles },
    });

    if (loading) return <div>Loading...</div>;
    if (error) return <div>Error!</div>;

    const sums = new Map();
    data.getSumCommentaries.map(e => sums.set(e.articleId, e.sum));

    return (
        <div className='home'>
            <h1>New Articles</h1>
            <List>
                {data.getLastNArticles.map((art, index) =>
                <ListItem key={index}>
                    <ArticleInsight article={art} numberCommentaries={sums.get(parseInt(art.id))} />
                </ListItem>)}
            </List>

        </div>
    );
}

export default Home;