import React from 'react';
import Truncate from "react-truncate";
import {Link} from "react-router-dom";
import {Box, Grid} from "@material-ui/core";

function ArticleInsight(props) {
    const art = props.article;
    const url = '/article/' + art.id;
    return (
        <div>
            <Grid item xs={12} sm={12}>
                <Box border={1} bg-color="secondary.main" p={2}>
                    <h2>{art.title}</h2>
                    <p>{Date(art.dateCreation)}, {props.numberCommentaries} commentaries, {art.category}</p>
                    <Truncate lines={3} ellipsis={<span>...</span>}>
                        {art.text}
                    </Truncate>
                    <br/>
                    <Link to={url}>Read more</Link>
                </Box>
            </Grid>

        </div>
    );
}

export default ArticleInsight;