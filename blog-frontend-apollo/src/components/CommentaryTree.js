import React from 'react';
import Tree from '@naisutech/react-tree'
import {DateTime} from "luxon";
import CommentaryForm from "./CommentaryForm";

function CommentaryTree(props) {
    const [commentaryId, setCommentaryId] = React.useState("1");
    const articleId = props.articleId;
    const treeComs = props.commentaries.map(c => new CommentaryNode(c));

    const onSelect = selectedNode => {
        setCommentaryId(selectedNode.id);
    }

    return (
        <div>
            <Tree nodes={treeComs} onSelect={onSelect} />
            <CommentaryForm articleId={articleId} parentId={commentaryId}/>
        </div>
    );
}

function CommentaryNode(commentary) {
    this.label = commentary.text + ' - ' + commentary.author + " @ " + DateTime.fromISO(commentary.date).toHTTP();
    this.commentary = commentary;
    this.id = commentary.id;
    this.parentId = commentary.parentId === 1 ? null : commentary.parentId.toString();
    return this;
}

export default CommentaryTree;