import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import Article from "./Article";
import AppAdmin from "./admin/AppAdmin";

function Main(props) {
    const blog = props.blog;
    const pathAdmin = '/' + blog.pathAdmin;
    return (
        <Switch>
            <Route exact path='/' render={(props) => (
                <Home {...props} blog={blog} />
            )}/>
            <Route exact path='/about' component={About}/>
            <Route exact path='/contact' render={(props) => (
                <Contact {...props} blog={blog} />
            )}/>
            <Route path={pathAdmin} render={(props) => (
                <AppAdmin {...props} blog={blog}/>
            )}/>
            <Route path="/article/:articleId" render={(props) => (
                <Article {...props} />
            )}/>
        </Switch>
    );
}

export default Main;