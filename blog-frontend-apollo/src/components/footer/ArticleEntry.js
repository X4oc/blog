import React from 'react';
import {Link} from "react-router-dom";
import {DateTime} from "luxon";

function ArticleEntry(props) {
    const art = props.article;
    const url = '/article/' + art.id;
    return (
        <div>
            <Link to={url}>{art.title}</Link> - {DateTime.fromISO(art.dateCreation).toHTTP()}
        </div>
    );
}

export default ArticleEntry;