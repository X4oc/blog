import React from 'react';
import {Link} from "react-router-dom";
import ListItemText from "@material-ui/core/ListItemText";
import {DateTime} from "luxon";

function CommentaryEntry(props) {
    const com = props.commentary;
    const url = '/article/' + com.articleId;
    return (
        <div>
            <ListItemText>
                <Link to={url}>{com.author}</Link>
                { } {DateTime.fromISO(com.date).toRelative()}
            </ListItemText>


        </div>
    );
}

export default CommentaryEntry;