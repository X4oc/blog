import React from 'react';
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import {Box, List, Grid} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import CommentaryEntry from "./CommentaryEntry";
import ArticleEntry from "./ArticleEntry";
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import BlogRoll from "./BlogRoll";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            backgroundColor: theme.palette.background.paper,
        },
        title: {
            margin: theme.spacing(1, 0, 1),
        },
    }),
);

function Footer() {
    const classes = useStyles();
    const GET_FOOTER_INFO = gql`
    query QueryFooter{
        getLastCommentaries {id, author, date, text, articleId, parentId},
        getLastArticles {id, title, dateCreation, dateLastModified, text, category},
        getBlogRollItems {id, title, url, category},
        getAllCategories
    }
    `;

    const { loading, error, data } = useQuery(GET_FOOTER_INFO);

    if (loading) return <div>Loading...</div>;
    if (error) return `Error! ${error.message}`;
    const lastCommentaries = data.getLastCommentaries;
    const lastArticles = data.getLastArticles;
    const blogRollItems = data.getBlogRollItems;
    const categories = data.getAllCategories;

    return (
        <div className={classes.root}>
            <Grid container spacing={1}>
                <Grid item xs={12} sm={6}>
                    <Box border={1} bg-color="secondary.main" p={2}>
                        <Typography variant="h6" className={classes.title}>
                            Last commentaries
                        </Typography>
                        <List dense>
                            {lastCommentaries.map((com, index) =>
                                <ListItem key={index}>
                                    <CommentaryEntry commentary={com}/>
                                </ListItem>
                            )}
                        </List>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Box border={1} bg-color="primary.main" p={2}>
                        <Typography variant="h6" className={classes.title}>
                            Last articles
                        </Typography>
                        <List dense>
                            {lastArticles.map((art, index) =>
                                <ListItem key={index}>
                                    <ArticleEntry article={art} />
                                </ListItem>
                            )}
                        </List>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Box border={1} bg-color="primary.main" p={2}>
                        <Typography variant="h6" className={classes.title}>
                            Categories
                        </Typography>
                        <List dense>
                            {categories.map((cat, index) =>
                                <ListItem key={index}>{cat}</ListItem>
                            )}
                        </List>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Box border={1} bg-color="primary.main" p={2}>
                        <Typography variant="h6" className={classes.title}>
                            Blog Roll
                        </Typography>
                        <BlogRoll blogRollItems={blogRollItems}/>
                    </Box>
                </Grid>
            </Grid>
        </div>
    );
}

export default Footer;