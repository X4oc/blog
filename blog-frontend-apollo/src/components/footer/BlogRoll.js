import React from 'react';
import ListItem from "@material-ui/core/ListItem";
import {List} from "@material-ui/core";
import ListItemText from '@material-ui/core/ListItemText';

function BlogRoll(props) {
    const items = props.blogRollItems;

    function groupByKey(list, key){
        return  list.reduce((hash, obj) => ({...hash, [obj[key]]:( hash[obj[key]] || [] ).concat(obj)}), {});
    }

    const groupItems = groupByKey(items, 'category');

    let lists = [];
    for(const group in groupItems){
        lists.push(
            <List subheader={group} key={group} dense>
                {groupItems[group].map(item =>
                    <ListItem key={item.id}>
                        <a href={item.url}>
                            <ListItemText>{item.title}</ListItemText>
                        </a>
                    </ListItem>)}
            </List>
        )
    }
    return (
        <div>
            {lists}
        </div>
    );
}

export default BlogRoll;