import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

function Navigation(props) {
    const useStyles = makeStyles({
        root: {
            flexGrow: 1,
        },
    });

    const classes = useStyles();
    const [value, setValue] = React.useState(false);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Paper className={classes.root}>
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                centered
            >
                <Tab label="Home" component={NavLink} to='/'/>
                <Tab label="About" component={NavLink} to='/about'/>
                <Tab label="Contact" component={NavLink} to='/contact'/>
                <Tab label="Twitter" component={Link} to={{ pathname: props.blog.twitter }} target="_blank"/>
            </Tabs>
        </Paper>
    );
}

export default Navigation;