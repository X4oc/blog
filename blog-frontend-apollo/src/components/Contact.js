import React from 'react';

function Contact(props) {
    return (
        <div className='contact'>
            <h1>Contact Me</h1>
            <p>You can reach me via email: {props.blog.email}</p>

        </div>
    );
}

export default Contact;