import { createMuiTheme } from '@material-ui/core/styles';
import pink from "@material-ui/core/colors/pink";
import {orange, cyan} from "@material-ui/core/colors";
import {colors} from "@material-ui/core";

export default createMuiTheme({
    palette: {
        primary: {
            light: orange["50"],
            main: orange["400"],
            dark: orange["900"],
            contrastText: orange["500"],
        },
        secondary: {
            light: cyan["50"],
            main: cyan["400"],
            dark: cyan["900"],
            contrastText: cyan["500"],
        },
        background: {
            paper: orange["50"],
        }
    },
    tableRow: {
        "&$hover:hover": {
            backgroundColor: "blue"
        }
    },
    tableCell: {
        "$hover:hover &": {
            color: "pink"
        }
    },
    hover: {}
});