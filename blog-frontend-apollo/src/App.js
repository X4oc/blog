import React from 'react';
import Navigation from "./components/Navigation";
import Main from "./components/Main";
import Footer from "./components/footer/Footer";
import {gql} from "apollo-boost";
import {useQuery} from "@apollo/client";
import {makeStyles, ThemeProvider as MuiThemeProvider} from '@material-ui/core/styles';
import theme from "./theme";
import AppBar from "@material-ui/core/AppBar";
import {Toolbar} from "@material-ui/core";

function App() {
    const useStyles = makeStyles({
        center: {
            justifyContent: 'center',
            alignItems: 'center',
            width: 'auto',
        },
    });

    const classes = useStyles();

    const GET_BLOG_PROPERTIES = gql`
        query GetBlogProperties{
            getBlogProperties{pathAdmin, email, twitter, rss, imgTitle, lastNArticles, lastNCommentaries, maxBlogRollItems
            , moderationCommentaryAuthor, moderationCommentaryText}
        }
    `;

    const { loading, error, data } = useQuery(GET_BLOG_PROPERTIES);

    if (loading) return <div>Loading...</div>;
    if (error) return `Error!3 ${error.message}`;

    const blog = data.getBlogProperties;

    return (
        <MuiThemeProvider theme={theme}>
            <div className="App">
                <AppBar position="static" width="auto">
                    <Toolbar className="classes.title">
                        <div className={classes.center}>
                            <img src={blog.imgTitle} alt="img title"/>
                        </div>
                    </Toolbar>
                </AppBar>
                <Navigation blog={blog}/>
                <Main blog={blog}/>
                <Footer />
            </div>
        </MuiThemeProvider>
    );
}

export default App;